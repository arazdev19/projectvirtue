package freelancer.araz.projectvirtue.view

import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.adapters.QuotesAdapter
import freelancer.araz.projectvirtue.viewmodel.QuotesViewModel
import kotlinx.android.synthetic.main.quotes_layout.*

class QuotesActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.quotes_layout)

        val quotesvm = ViewModelProviders.of(this).get(QuotesViewModel::class.java)
        quotesvm.getQuotesList(applicationContext)
        quotesvm.mutableQuotesList.observe(this, Observer {
            val quotesAdapter = QuotesAdapter(it,applicationContext)
            val quotesLinearManager =LinearLayoutManager(applicationContext)
            quotes_recycler.layoutManager = quotesLinearManager
            quotes_recycler.adapter = quotesAdapter
        })
    }
}