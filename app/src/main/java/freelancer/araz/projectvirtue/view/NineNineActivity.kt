package freelancer.araz.projectvirtue.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.adapters.NineListAdapter
import freelancer.araz.projectvirtue.viewmodel.NineNineViewModel
import kotlinx.android.synthetic.main.nine_layout.*

class NineNineActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nine_layout)

        val ninevm = ViewModelProviders.of(this).get(NineNineViewModel::class.java)
        ninevm.getNineJsonArray(applicationContext)
        ninevm.nineJsonMutable.observe(this, Observer {
            val nineadapter = NineListAdapter(it)
            val ninelayoutmanager = LinearLayoutManager(applicationContext)
            nine_recycler_view.layoutManager = ninelayoutmanager
            nine_recycler_view.adapter = nineadapter
        })
    }
}