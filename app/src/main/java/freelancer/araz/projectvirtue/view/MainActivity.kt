package freelancer.araz.projectvirtue.view

import android.animation.AnimatorSet
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.time.Period
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var quotesHandler:Handler
    lateinit var quotesRunnable:Runnable

    lateinit var zikirHandler:Handler
    lateinit var zikirRunnable:Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(applicationContext,getString(R.string.appaddid))
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        val mainvm = ViewModelProviders.of(this).get(MainViewModel::class.java)
        quotesHandler = Handler()
        zikirHandler = Handler()

        quotesTimer(mainvm)
        zikirTimer(mainvm)

        mainvm.mutableQuotes.observe(this, Observer {
            val quotesTitle = it.getString("quotes")
            val fadeInAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.fade_in)
            val fadeOutAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.fade_out)
            random_quotes.startAnimation(fadeOutAnimation)

            quotesHandler.removeCallbacks(quotesRunnable)
            quotesTimer(mainvm)

            fadeOutAnimation.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    random_quotes.text = "''${quotesTitle}''"
                    random_quotes.startAnimation(fadeInAnimation)
                }

                override fun onAnimationStart(animation: Animation?) {

                }

            })


        })

        mainvm.mutableZikir.observe(this, Observer {
            val zikirTitle = it.getString("title")
            val zikirTrans = it.getString("trans")
            val fadeInAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.fade_in)
            val fadeOutAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.fade_out)
            zikir_text.startAnimation(fadeOutAnimation)
            zikir_trans.startAnimation(fadeOutAnimation)

            zikirHandler.removeCallbacks(zikirRunnable)
            zikirTimer(mainvm)

            fadeOutAnimation.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    zikir_text.text = "''${zikirTitle}''"
                    zikir_trans.text = zikirTrans

                    zikir_text.startAnimation(fadeInAnimation)
                    zikir_trans.startAnimation(fadeInAnimation)
                }

                override fun onAnimationStart(animation: Animation?) {

                }

            })


        })

        flt_btn_doa.setOnClickListener {
            val intent = Intent(this, DoaListActivity::class.java)
            startActivity(intent)
        }

        flt_btn_99_names.setOnClickListener {
            val intent = Intent(this,NineNineActivity::class.java)
            startActivity(intent)
        }

        quotes_refresh_btn.setOnClickListener {
            mainvm.randomQuotes(applicationContext)
        }

        zikir_refresh_btn.setOnClickListener {
            mainvm.randomZikir(applicationContext)
        }

        flt_btn_quotes.setOnClickListener {
            val intent = Intent(this,QuotesActivity::class.java)
            startActivity(intent)
        }

        flt_btn_syahadah.setOnClickListener {
            val intent = Intent(this, ShahadahActivity::class.java)
            startActivity(intent)
        }
    }

    fun quotesTimer(mainvm:MainViewModel){
        quotesRunnable = Runnable {
            mainvm.randomQuotes(applicationContext)
            quotesHandler.postDelayed(quotesRunnable,10000)
        }

        quotesHandler.postDelayed(quotesRunnable,10000)
    }

    fun zikirTimer(mainvm: MainViewModel){
        zikirRunnable = Runnable {
            mainvm.randomZikir(applicationContext)
            zikirHandler.postDelayed(zikirRunnable,10000)
        }


        zikirHandler.postDelayed(zikirRunnable,10000)
    }
}
