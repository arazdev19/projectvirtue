package freelancer.araz.projectvirtue.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import freelancer.araz.projectvirtue.R
import kotlinx.android.synthetic.main.shahadah_layout.*

class ShahadahActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shahadah_layout)

        MobileAds.initialize(application,getString(R.string.appaddid))
        val adRequest = AdRequest.Builder().build()
        adviewshahadah.loadAd(adRequest)
    }

}