package freelancer.araz.projectvirtue.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.adapters.DoaListAdapter
import freelancer.araz.projectvirtue.viewmodel.DoaListViewModel
import kotlinx.android.synthetic.main.doa_list_fragment.*

class DoaListActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.doa_list_fragment)

        val doalistvm = ViewModelProviders.of(this).get(DoaListViewModel::class.java)
        doalistvm.getDoaJsonArray(applicationContext)
        doalistvm.doaJsonArray.observe(this, Observer {
            // Populate doa list
            val doaadapter = DoaListAdapter(it,applicationContext)
            val lm = LinearLayoutManager(applicationContext)
            doa_recycler.layoutManager = lm
            doa_recycler.adapter = doaadapter
        })

        // Some firebase test
        doalistvm.fireBaseTest()
        doalistvm.mtbfirebase.observe(this, Observer {

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}