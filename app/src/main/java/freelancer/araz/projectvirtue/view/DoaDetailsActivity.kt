package freelancer.araz.projectvirtue.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.viewmodel.DoaDetailsViewModel
import kotlinx.android.synthetic.main.doa_details_layout.*
import org.json.JSONArray
import org.json.JSONObject

class DoaDetailsActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.doa_details_layout)

        MobileAds.initialize(applicationContext,getString(R.string.appaddid))
        val adRequest = AdRequest.Builder().build()
        adviewdoadetails.loadAd(adRequest)

        val doadetailsvm = ViewModelProviders.of(this).get(DoaDetailsViewModel::class.java)
        doadetailsvm.processDoaJsonObject(JSONObject(intent.getStringExtra("doajson")))
        doadetailsvm.mutabledoajsonobject.observe(this, Observer {
            doa_title.text = it.getString("doa_title")
            doa_trans.text = it.getString("trans_my")
            doa_img.setImageDrawable(resources.getDrawable(resources.getIdentifier(it.getString("id"),"drawable",packageName)))
        })

    }
}