package freelancer.araz.projectvirtue.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import freelancer.araz.projectvirtue.R
import freelancer.araz.projectvirtue.view.DoaDetailsActivity
import kotlinx.android.synthetic.main.doa_row.view.*
import org.json.JSONArray
import java.util.zip.Inflater

class DoaListAdapter(val doajson:JSONArray,val c: Context):RecyclerView.Adapter<DoaListAdapter.DoaListViewHolder>() {

    inner class DoaListViewHolder(view:View):RecyclerView.ViewHolder(view){
        var doatitletext = view.doa_title
        val doacard = view.doa_card
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoaListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.doa_row,parent,false)
        return DoaListViewHolder(v)
    }

    override fun getItemCount(): Int {
        return doajson.length()
    }

    override fun onBindViewHolder(holder: DoaListViewHolder, position: Int) {
        val doajsonobject = doajson.getJSONObject(position)
        val doatitle = doajsonobject.getString("doa_title")
        holder.doatitletext.text = doatitle
        holder.doacard.setOnClickListener {
            val intent = Intent(c,DoaDetailsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("doajson",doajsonobject.toString())
            c.startActivity(intent)
        }
    }



}