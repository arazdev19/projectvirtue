package freelancer.araz.projectvirtue.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.json.JSONArray
import freelancer.araz.projectvirtue.R
import kotlinx.android.synthetic.main.quotes_row.view.*

class QuotesAdapter(val quotesJsonArray:JSONArray,val c:Context):RecyclerView.Adapter<QuotesAdapter.QuotesViewHolder>() {

    inner class QuotesViewHolder(view: View):RecyclerView.ViewHolder(view){
        val quotesDesc = view.quotes_desc
        val quotesAuthor = view.quotes_author
        val quotesBg = view.quotes_bg
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.quotes_row,parent,false)
        return QuotesViewHolder(v)
    }

    override fun getItemCount(): Int {
        return quotesJsonArray.length()
    }

    override fun onBindViewHolder(holder: QuotesViewHolder, position: Int) {
        val quotesJsonObject = quotesJsonArray.getJSONObject(position)
        val quotesDesvVal = quotesJsonObject.getString("quotes")
        val quotesAuthorVal = quotesJsonObject.getString("author")
        val imgName = "b" + (1 until 20).shuffled().first()
        holder.quotesDesc.text = "''$quotesDesvVal''"
        holder.quotesAuthor.text = quotesAuthorVal
        Picasso.get().load(c.resources.getIdentifier(imgName,"mipmap",c.packageName)).into(holder.quotesBg)
    }
}