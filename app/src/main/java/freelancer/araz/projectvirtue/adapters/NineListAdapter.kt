package freelancer.araz.projectvirtue.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import freelancer.araz.projectvirtue.R
import kotlinx.android.synthetic.main.nine_row.view.*

class NineListAdapter(val nineJson:JSONArray):RecyclerView.Adapter<NineListAdapter.NineViewHolder>() {

    inner class NineViewHolder(view:View):RecyclerView.ViewHolder(view){
        val nineTitle = view.nine_title
        val nineDesc = view.nine_desc
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NineViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.nine_row,parent,false)
        return NineViewHolder(v)
    }

    override fun getItemCount(): Int {
        return nineJson.length()
    }

    override fun onBindViewHolder(holder: NineViewHolder, position: Int) {
        val nineJsonObject = nineJson.getJSONObject(position)
        val title = nineJsonObject.getString("name")
        val desc = nineJsonObject.getString("desc")

        holder.nineTitle.text = title
        holder.nineDesc.text = desc

    }
}