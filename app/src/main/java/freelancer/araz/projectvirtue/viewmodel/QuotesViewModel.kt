package freelancer.araz.projectvirtue.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONArray
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.Exception

class QuotesViewModel:ViewModel() {

    val mutableQuotesList = MutableLiveData<JSONArray>()

    fun getQuotesList(c:Context){
        try {
            val quotesInputStream = c.assets.open("quotes.json")
            val quotesBufferedStreamReader = BufferedReader(InputStreamReader(quotesInputStream))
            val quotesResult = quotesBufferedStreamReader.readText()
            val qutoesJsonArray = JSONArray(quotesResult)
            mutableQuotesList.value = qutoesJsonArray
        }catch (e:Exception){
            mutableQuotesList.value = null
        }
    }

}