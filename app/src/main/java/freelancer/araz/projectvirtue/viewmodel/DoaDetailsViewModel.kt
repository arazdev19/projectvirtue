package freelancer.araz.projectvirtue.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONObject

class DoaDetailsViewModel:ViewModel() {

    val mutabledoajsonobject = MutableLiveData<JSONObject>()

    fun processDoaJsonObject(doajson:JSONObject){
        mutabledoajsonobject.value = doajson
    }

}