package freelancer.araz.projectvirtue.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStreamReader
import java.lang.Exception
import java.util.*

class MainViewModel:ViewModel() {

    val mutableQuotes = MutableLiveData<JSONObject>()
    val mutableZikir = MutableLiveData<JSONObject>()
    val viewModelJob = Job()
    val mainUiScope = CoroutineScope(Dispatchers.Main + viewModelJob)


    fun randomQuotes(c:Context){
        try{
            val quotesStream = c.assets.open("quotes.json")
            val quotesStreamReader = InputStreamReader(quotesStream)
            val quotesResult = quotesStreamReader.readText()
            val quotesJsonArray = JSONArray(quotesResult)
            val quotesLength = quotesJsonArray.length()
            val randToss = (0 until quotesLength-1).shuffled().first()
            val quotesJsonObject = quotesJsonArray.getJSONObject(randToss)
            mutableQuotes.value = quotesJsonObject
        }catch (e:Exception){
            mutableQuotes.value = null
        }
    }

    fun randomZikir(c:Context){
        try{
            val zikirStream = c.assets.open("zikir.json")
            val zikirStreamReader = InputStreamReader(zikirStream)
            val zikirResult = zikirStreamReader.readText()
            val zikirJsonArray = JSONArray(zikirResult)
            val zikirLength = zikirJsonArray.length()
            val randToss = (0 until zikirLength-1).shuffled().first()
            val zikirJsonObject = zikirJsonArray.getJSONObject(randToss)
            mutableZikir.value = zikirJsonObject
        }catch (e:Exception){
            mutableZikir.value = null
        }
    }

}