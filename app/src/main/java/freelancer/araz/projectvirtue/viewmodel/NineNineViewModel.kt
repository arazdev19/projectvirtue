package freelancer.araz.projectvirtue.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.Exception

class NineNineViewModel:ViewModel() {

    val nineJsonMutable = MutableLiveData<JSONArray>()

    fun getNineJsonArray(c:Context){
        try {
            val nineStream = c.assets.open("name99.json")
            val nineBufferedStreamReader = BufferedReader(InputStreamReader(nineStream))
            val nineJsonStr = nineBufferedStreamReader.readText()
            val nineJsonArray = JSONArray(nineJsonStr)
            nineJsonMutable.value = nineJsonArray
        }catch (e:Exception){
            nineJsonMutable.value = null
        }

    }

}