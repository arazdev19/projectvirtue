package freelancer.araz.projectvirtue.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.Exception

class DoaListViewModel:ViewModel() {

    val doaJsonArray = MutableLiveData<JSONArray>()
    val mtbfirebase = MutableLiveData<String>()

    fun getDoaJsonArray(c: Context){
        try{
            val doaInputStream:InputStream = c.assets.open("doa.json")
            val doastreamreader = BufferedReader(InputStreamReader(doaInputStream))
            val doaresult = doastreamreader.readText()
            val doajsonarrayresult = JSONArray(doaresult)
            doaJsonArray.value = doajsonarrayresult
        }catch (e:Exception){
            doaJsonArray.value = null
        }
    }

    fun fireBaseTest(){
        val fb = FirebaseDatabase.getInstance()
        val dbRef = fb.getReference("msg")

        dbRef.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                mtbfirebase.value = p0.toString()
            }

            override fun onDataChange(snap: DataSnapshot) {
                val v = snap.getValue(String::class.java)
                val m = snap.childrenCount

                snap.children.forEach {
                    val k = it.child("salary").value.toString()
                }
                mtbfirebase.value = v
            }

        })

    }

}