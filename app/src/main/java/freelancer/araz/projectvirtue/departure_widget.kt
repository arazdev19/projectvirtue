package freelancer.araz.projectvirtue

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import freelancer.araz.projectvirtue.view.MainActivity
import freelancer.araz.projectvirtue.viewmodel.QuotesViewModel
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception

/**
 * Implementation of App Widget functionality.
 */
class departure_widget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            updateQuotes(context,appWidgetManager,appWidgetId)
        }
    }

    fun updateQuotes(context: Context,appWidgetManager: AppWidgetManager,appWidgetId: Int){
        val views = RemoteViews(context.packageName, R.layout.departure_widget)

        val quotesResult = try {
            val randomQuotes = context.assets.open("quotes.json")
            val randomQuotesReader = BufferedReader(InputStreamReader(randomQuotes))
            val randomQuotesStr = randomQuotesReader.readText()
            val quotesJsonArray = JSONArray(randomQuotesStr)
            val quotesLength = quotesJsonArray.length()
            val randomInt = (0 until  quotesLength-1).shuffled().first()
            val quotesJsonObject = quotesJsonArray.getJSONObject(randomInt)
            val quotesDesc = quotesJsonObject.getString("quotes")
            quotesDesc
        }catch (e:Exception){
            "You are the one who can decide, if you are going to have a good day or not."
        }

        views.setTextViewText(R.id.appwidget_text, quotesResult)

        val openAppIntent = Intent(context,MainActivity::class.java)
        val openAppPendingIntent = PendingIntent.getActivity(context,0,openAppIntent,0)
        views.setOnClickPendingIntent(R.id.widget_main_layout,openAppPendingIntent)

        views.setOnClickPendingIntent(R.id.refresh_btn,getRefreshPendingIntent(context,appWidgetId))

        appWidgetManager.updateAppWidget(appWidgetId, views)
        Toast.makeText(context,quotesResult,Toast.LENGTH_SHORT).show()
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val views = RemoteViews(context.packageName, R.layout.departure_widget)
        if(intent.action == "android.appwidget.action.APPWIDGET_UPDATE"){
            val quotesResult = try {
                val randomQuotes = context.assets.open("quotes.json")
                val randomQuotesReader = BufferedReader(InputStreamReader(randomQuotes))
                val randomQuotesStr = randomQuotesReader.readText()
                val quotesJsonArray = JSONArray(randomQuotesStr)
                val quotesLength = quotesJsonArray.length()
                val randomInt = (0 until  quotesLength-1).shuffled().first()
                val quotesJsonObject = quotesJsonArray.getJSONObject(randomInt)
                val quotesDesc = quotesJsonObject.getString("quotes")
                quotesDesc
            }catch (e:Exception){
                "You are the one who can decide, if you are going to have a good day or not."
            }
            views.setTextViewText(R.id.appwidget_text,quotesResult)
        }

        val componentName = ComponentName(context,departure_widget::class.java)
        AppWidgetManager.getInstance(context).updateAppWidget(componentName,views)

    }

    override fun onEnabled(context: Context) {

    }

    override fun onDisabled(context: Context) {

    }

    fun getRefreshPendingIntent(context: Context,appWidgetId: Int):PendingIntent{
        val intent = Intent("android.appwidget.action.APPWIDGET_UPDATE")
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetId)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        return PendingIntent.getBroadcast(context,0, intent,PendingIntent.FLAG_UPDATE_CURRENT)
    }

}

